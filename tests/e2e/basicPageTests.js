module.exports = {
  'Home page test': (client) => {
    const page = client.page.home();

    page.navigate()
      .assert.title('@title')
      .assert.elementPresent('.c-siteHeader')
      .assert.elementPresent('main')
      .assert.elementPresent('.c-siteFooter')
      .assert.elementPresent('.c-primaryNav')
      .assert.elementPresent('.c-secondaryNav')
      .assert.elementPresent('.c-imageGrid')
      .assert.elementPresent('.c-textCols');

    client.end();
  }
};
