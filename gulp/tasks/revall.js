// TASK - REVALL - revisions all assets

'use strict';

import gulp         from 'gulp';
import gutil        from 'gulp-util';
import plumber      from 'gulp-plumber';
import RevAll       from 'gulp-rev-all';

import { config } from '../config/revall';
import { onError } from '../config/_global';


gulp.task('revAll', () => {
    gulp.src(config.src)
      .pipe(plumber(onError))
      .pipe(RevAll.revision({
        dontRenameFile: config.dontRename,
        dontUpdateReference: config.dontUpdateReference
      }))
      .pipe(gulp.dest(config.dest));
});
