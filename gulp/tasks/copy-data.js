// TASK - Copy data

'use strict';

import gulp           from 'gulp';

import { config } from '../config/copy-data';

gulp.task('copyData', () => {
  return gulp
    .src(config.src)
    .pipe(gulp.dest(config.dest))
});
