'use strict';

export const config = {
  a: 'views',
  b: [
    'css',
    'js',
    'images:css',
    'images',
    'copyData',
    'htmlValidation',
  ],
  c: [
    'revAll'
  ]
};
