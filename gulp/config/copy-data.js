'use strict';

import { path } from './_global';

export const config = {
  src: `${path.src}/data/*`,
  dest: `${path.dev}/data`
}
