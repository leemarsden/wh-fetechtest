# WH-TechTest
WH Technical test for Front end Development

##Technologies used
-Templating - Nunjucks
-CSS - Sass and PostCSS(autoprefixer, postCssVr, postCssPxtoRem, postCssFocus, cssMqPacker, postCssNano)
-JS - Babel
-Build - Gulp
-E2E testing - nightwatch

##Project setup

To run the site you need to have a few things installed and setup:

###Installs
####Node.js and NPM
Browse to the following URL https://nodejs.org/en/ and download the latest stable release for your platform
Once downloaded run the setup keeping defaults
To verify both node and NPM are installed open up the terminal and run 'node-v' and or 'npm -v', if installed this will return the version installed

####Gulp
Gulp needs to be installed globally to enable you to run the gulp command from any directory.
Open up the terminal and run 'npm install gulp -g'

###setup
Clone the repository to a location of your choice
cd into the cloned repository
run 'npm install'

###running the site
cd into the directory where the project files are and run:

npm run build - this will run all the gulp tasks and output a dev build to the 'dev' directory

npm run dev - this will run the above but also start a local webserver on 'http://localhost:3000' with watch and auto browser reload - used while developing.

npm run prod - this will run all the gulp tasks but produce a production build by minifying assets as well as revision them for caching.

To run an individual task, type 'gulp' followed by the task name e.g. 'gulp js'.

###running tests

To run the End to End testing you also need to install the Java Development kit installed http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

npm run e2e - The site will need to be running on 'http://localhost:3000'

All the E2E tests currently do is check for certain elements, if I have more time I would look to expand on these tests for further confidence in the code I am building.

###Notes
Development work was done on my MAC, this posed a challenge in testing for Internet Explorer browsers, I have written code I believe should work across these browsers but cannot be 100% confident in this. If I had further time on a task such as this I would have worked with each browser while developing to ensure all worked as expected.
