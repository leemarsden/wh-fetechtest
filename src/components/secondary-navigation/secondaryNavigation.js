// Function to toggle the primary site navigation

export default () => {
  'use strict';

  // Grab the element to add the click handler to
  const $el = document.getElementById('js-toggleSecondaryNav');

  // if the element is not available exit the funciton
  if (!$el) return;

  const $body = document.getElementsByTagName('body')[0];

  $el.addEventListener('click', function() {
    this.classList.toggle('isActive');
    $body.classList.toggle('secondaryNav-IsActive');
  });

}
