// Function to toggle the primary site navigation

export default () => {
  'use strict';

  // Grab the element to add the click handler to
  const $el = document.getElementById('js-togglePrimaryNav');

  // if the element is not available exit the funciton
  if (!$el) return;

  // Grab the element to make visible from the data attribute
  const $elToToggle = document.getElementById($el.getAttribute('data-target'));

  $el.addEventListener('click', function() {
    this.classList.toggle('isActive');
    $elToToggle.classList.toggle('isActive');
  });

};
