'use strict';

import primaryNavigation from "../components/primary-navigation/primaryNavigation";
import secondaryNavigation from "../components/secondary-navigation/secondaryNavigation";
import sticky from './modules/sticky';
import menu from './modules/menu'

primaryNavigation();
secondaryNavigation();
sticky('js-secondaryNav');
menu();
