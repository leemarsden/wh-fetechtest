// Add sticky class to element to keep fixed while scrolling

export default (selector) => {
  'use strict';

  //
  const $el = document.getElementById(selector);

  if(!$el) return;

  const $elTop = $el.offsetTop;

  const checkScroll = () => {
    const scrollTop = window.pageYOffset;
    if(scrollTop >= $elTop) {
      $el.classList.add('isSticky');
    }else {
      $el.classList.remove('isSticky');
    }
  };

  window.addEventListener('scroll', checkScroll);
};
