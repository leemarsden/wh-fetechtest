export default (selector) => {

  // Content is loaded into this element
  const content = document.getElementById('content');

  if (!content) return;

  //Create request for menu.json file
  const xhr = new XMLHttpRequest();
  xhr.open('GET', 'data/menu.json', true);
  xhr.onreadystatechange = function () {

    if (xhr.readyState === 4 && xhr.status === 200) {
      const data = JSON.parse(xhr.responseText);
      const menu = new Menu(data.menu);
      const ul = menu.render();

      // Using event propagation to keep down number of event handlers
      ul.addEventListener('click', function(e) {
        const el = e.target;

        if(el.nodeName === 'LI') {

          if(el.getAttribute('data-leaf') === 'true') {
            // If this is a leaf, render the content
            content.textContent = el.getAttribute('data-content');

            return; // We're done!
          }

          // If this isn't a leaf, find the first child menu
          const ul = el.getElementsByTagName('ul')[0];

          // Toggle the HTML hidden attribute to show/hide this menu
          const isHidden = ul.hasAttribute('hidden');

          if (isHidden) {
            ul.removeAttribute('hidden');
          } else {
            ul.setAttribute('hidden', 'hidden');
          }

        }

      });

      // Add the rendered menu to the DOM.
      document.getElementById('menu').appendChild(ul);

    }

  };
  xhr.send();

  // A full menu of items.
  // @param items  - the array of item objects (items as per the format used in the provided JSON)
  // @param isSubmenu - true if this is a child menu (default false)
  function Menu(items, isSubmenu) {
    this.items = items;
    this.isSubmenu = isSubmenu || false;
  }

  // Render this menu, including all items within it.
  Menu.prototype.render = function () {

    const ul = document.createElement('ul');

    // Submenus are hidden by default
    if (this.isSubmenu) { ul.setAttribute('hidden', 'hidden'); }

    // Render all items within this menu
    this.items.forEach(function (item) {
      const menuItem = new MenuItem(item);
      ul.appendChild(menuItem.render())
    });

    return ul;

  }

  // A single menu item (non-leaf items may contain submenus)
  // @param item - the menu item (as per the format used in the provided JSON)
  function MenuItem (item) {
    this.id = item.id;
    this.cssClass = item.cssClass;
    this.isLeaf = item.leaf;
    this.description = item.description;
    this.content = item.content;
    this.menu = item.menu;
  }

  // Render this menu item, including submenu for non-leaf nodes.
  MenuItem.prototype.render = function () {

    const li = document.createElement('li');

    li.id = this.id;
    li.className = this.cssClass;
    li.setAttribute('data-leaf', this.isLeaf);
    li.setAttribute('data-content', this.content);
    li.textContent = this.description;

    // Render submenu for non-leaf nodes
    if (!this.isLeaf) {
      const menu = new Menu(this.menu, true);
      li.appendChild(menu.render());
    }

    return li;

  }

};
